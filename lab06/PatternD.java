//David Ruiz
//CSE 002
//Professor Carr
//This program encompasses part D of Lab 6

import java.util.Scanner;

public class PatternD {

    public static void main(String[] args) {
        //Scanner initialization
        Scanner scanner = new Scanner(System.in);
        
        //Variables
        int rows;
        int i;
        int j;
        String pattern = "";
        String space = " ";
     
        //Terminal
        System.out.println("How many rows would you like?");
       
        //Number of rows
        do {
            System.out.println("Please enter an integer between 0 and 10.");
            while (!scanner.hasNextInt()) {
                System.out.println("Please enter an integer.");
                scanner.next();
            }
            rows = scanner.nextInt();
        } while (rows < 1 || rows > 9);
        
        //Row printing
        for(i = rows; i >= 1; i--){
            for(j = i; j >= 1; j--){
                pattern = pattern + String.valueOf(j) + space;
            }
            System.out.println(pattern);
            pattern = "";
        }
        
    }
    
}