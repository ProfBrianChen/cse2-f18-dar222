//David Ruiz
//CSE 002
//Professor Carr
//This program encompasses part C of Lab 6

import java.util.Scanner;

public class PatternC {

    public static void main(String[] args) {
        //Scanner initialization
        Scanner scanner = new Scanner(System.in);
        
        //Variables
        int rows;
        int i;
        String pattern = "";
        String space = " ";
        
        //Terminal
        System.out.println("How many rows would you like?");
       
        //Number of rows
        do {
            System.out.println("Please enter an integer between 0 and 10.");
            while (!scanner.hasNextInt()) {
                System.out.println("Please enter an integer.");
                scanner.next(); 
            }
            rows = scanner.nextInt();
        } while (rows < 1 || rows > 9);
        
        //Row printing
        for(i = 1; i <= rows; i++){
            pattern = String.valueOf(i) + space + pattern;
            String patternright = String
                    .format("%1$18s", pattern);
            System.out.println(patternright);
        }
        
    }
    
}