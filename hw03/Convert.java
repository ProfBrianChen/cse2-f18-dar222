//David Ruiz
//CSE 002
//Professor Carr
//The purpose of this program is to take input in the form of acres of land, and inches in rain, in order to convert those two values to cubic miles, and printing it to the terminal. 

//Imports scanner class
import java.util.Scanner; 

public class Convert {
  
  //Main method required for every program 
  public static void main(String[] args) {
    
    //Variables
    double acres;
    double rain;
    double acreinches;
    double gallons;
    double cubicmiles;
    
    //Scanner initialization
    Scanner scanner = new Scanner(System.in);
        
    //User-terminal interaction
    System.out.println("Enter the amount of acres in question: ");
    acres = scanner.nextDouble();
        
    System.out.println("Enter the amount of rain, in inches, in question: ");
    rain = scanner.nextDouble();
        
    //Calculations
    acreinches = acres * rain;
    gallons = acreinches * 27154;
    cubicmiles = gallons * (9.08169E-13);
        
    //Prints the amount of rainfall in cubic miles
    System.out.println("Amount of rainfall in cubic miles: " + cubicmiles);
    
  }//End of main method
  
}//End of main class