//David Ruiz
//CSE 002
//Professor Carr
//The purpose of this program is to take input in the form of the length, width, and height, of a pyramid, and output the volume of the pyramid. 

//Imports scanner class
import java.util.Scanner; 

public class Pyramid {

  //Main method required for every program
  public static void main(String[] args) {

    //Variables
    double length;
    double width;
    double height;
    double pyramidVolume;
        
    //Initialize scanner
    Scanner scanner = new Scanner(System.in);
        
    //User-terminal interaction
    System.out.println("Enter the length of the pyramid: ");
    length = scanner.nextDouble();
        
    System.out.println("Enter the width of the pyramid: ");
    width = scanner.nextDouble();
        
    System.out.println("Enter the height of the pyramid: ");
    height = scanner.nextDouble();
        
    //Calculations
    pyramidVolume = (length * width * height) / 3;
        
    //Prints the volume inside the pyramid
    System.out.println("The volume inside the pyramid is: " + pyramidVolume);
        
  }//End of main method
    
}//End of main class