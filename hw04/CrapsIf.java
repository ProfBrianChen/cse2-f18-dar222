import java.util.Scanner;

public class CrapsIf {

    public static void main(String[] args) {
        //Variables
        String choice;
        int die1 = 0;
        int die2 = 0;
        //Scanner initialization
        Scanner scanner = new Scanner(System.in);
        
        //User chooses between random dice roll generation or chooses the die
        System.out.println("Welcome! If you would like to choose the two dice, type 'choose'. If you would like to get two random dice, type 'roll'.");
        choice = scanner.nextLine();
        
        //If else for dice values
        if("roll".equals(choice) || "Roll".equals(choice)){
            
            //Random dice generation
            die1 = (int)(Math.random()*6)+1;
            die2 = (int)(Math.random()*6)+1;
            
        } else if("choose".equals(choice) || "Choose".equals(choice)){
            
            //User chooses dice
            do{
                System.out.print("Choose the value, as an integer between 0 to 6, of die 1: ");
                die1 = scanner.nextInt();
            } while(die1 < 0 || die1 ==0 || die1 > 6);
            
            do{
                System.out.print("Choose the value, as an integer between 0 to 6, of die 2: ");
                die2 = scanner.nextInt();
            } while(die2 < 0 || die2 ==0 || die2 > 6);
            
        } else {
            System.err.println("Please type either of the given strings, start again.");
        }
        
        System.out.print("You rolled a " + die1 + " and a " + die2 +". ");
        
        if(die1 == 1 && die2 == 1){
            System.out.println("You got Snake Eyes!");
        } else if(die1 == 1 && die2 == 2){
            System.out.println("You got Ace Deuce!");
        } else if(die1 == 1 && die2 == 3){
            System.out.println("You got Easy Four!");
        } else if(die1 == 1 && die2 == 4){
            System.out.println("You got Fever Five!");
        } else if(die1 == 1 && die2 == 5){
            System.out.println("You got Easy Six!");
        } else if(die1 == 1 && die2 == 6){
            System.out.println("You got Seven Out!");
        } else if(die1 == 2 && die2 == 1){
            System.out.println("You got Ace Deuce!");
        } else if(die1 == 2 && die2 == 2){
            System.out.println("You got Hard Four!");
        } else if(die1 == 2 && die2 == 3){
            System.out.println("You got Fever Five!");
        } else if(die1 == 2 && die2 == 4){
            System.out.println("You got Easy Six!");
        } else if(die1 == 2 && die2 == 5){
            System.out.println("You got Seven Out!");
        } else if(die1 == 2 && die2 == 6){
            System.out.println("You got Easy Eight!");
        } else if(die1 == 3 && die2 == 1){
            System.out.println("You got Easy Four!");
        } else if(die1 == 3 && die2 == 2){
            System.out.println("You got Fever Five!");
        } else if(die1 == 3 && die2 == 3){
            System.out.println("You got Hard Six!");
        } else if(die1 == 3 && die2 == 4){
            System.out.println("You got Seven Out!");
        } else if(die1 == 3 && die2 == 5){
            System.out.println("You got Easy Eight!");
        } else if(die1 == 3 && die2 == 6){
            System.out.println("You got Nine!");
        } else if(die1 == 4 && die2 == 1){
            System.out.println("You got Fever Five!");
        } else if(die1 == 4 && die2 == 2){
            System.out.println("You got Easy Six!");
        } else if(die1 == 4 && die2 == 3){
            System.out.println("You got Seven Out!");
        } else if(die1 == 4 && die2 == 4){
            System.out.println("You got Hard Eight!");
        } else if(die1 == 4 && die2 == 5){
            System.out.println("You got Nine!");
        } else if(die1 == 4 && die2 == 6){
            System.out.println("You got Easy Ten!");
        } else if(die1 == 5 && die2 == 1){
            System.out.println("You got Easy Six");
        } else if(die1 == 5 && die2 == 2){
            System.out.println("You got Seven Out");
        } else if(die1 == 5 && die2 == 3){
            System.out.println("You got Easy Eight");
        } else if(die1 == 5 && die2 == 4){
            System.out.println("You got Nine");
        } else if(die1 == 5 && die2 == 5){
            System.out.println("You got Hard Ten!");
        } else if(die1 == 5 && die2 == 6){
            System.out.println("You got Yo-leven!");
        } else if(die1 == 6 && die2 == 1){
            System.out.println("You got Seven Out!");
        } else if(die1 == 6 && die2 == 2){
            System.out.println("You got Easy Eight!");
        } else if(die1 == 6 && die2 == 3){
            System.out.println("You got Nine");
        } else if(die1 == 6 && die2 == 4){
            System.out.println("You got Easy Ten");
        } else if(die1 == 6 && die2 == 5){
            System.out.println("You got Yo-leven");
        } else if(die1 == 6 && die2 == 6){
            System.out.println("You got Boxcars!");
        }
        
    }
    
}
