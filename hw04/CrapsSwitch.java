import java.util.Scanner;

public class CrapsSwitch {

    public static void main(String[] args) {
        //Variables
        String choice;
        int die1 = 0;
        int die2 = 0;
        //Scanner initialization
        Scanner scanner = new Scanner(System.in);
        
        //User chooses between random dice roll generation or chooses the die
        System.out.println("Welcome! If you would like to choose the two dice, type 'choose'. If you would like to get two random dice, type 'roll'.");
        choice = scanner.nextLine();
        
        switch (choice) {
            case "roll":
            case "Roll":
                //Random dice generation
                die1 = (int)(Math.random()*6)+1;
                die2 = (int)(Math.random()*6)+1;
                break;
            case "choose":
            case "Choose":
                //User chooses dice
                do{
                    System.out.print("Choose the value, as an integer between 0 to 6, of die 1: ");
                    die1 = scanner.nextInt();
                } while(die1 < 0 || die1 ==0 || die1 > 6);
                do{
                    System.out.print("Choose the value, as an integer between 0 to 6, of die 2: ");
                    die2 = scanner.nextInt();
                } while(die2 < 0 || die2 ==0 || die2 > 6);
                break;
            default:
                System.err.println("Please type either of the given strings, start again.");
                break;
        }
        
        System.out.print("You rolled a " + die1 + " and a " + die2 +". ");
        
        switch (die1) {
            case 1:  
                switch (die2) {
                    case 1: 
                        System.out.println("You got Snake Eyes!");
                        break;
                    case 2:
                        System.out.println("You got Ace Deuce!");
                        break;
                    case 3:
                        System.out.println("You got Easy Four!");
                        break;
                    case 4:
                        System.out.println("You got Fever Five!");
                        break;
                    case 5:
                        System.out.println("You got Easy Six!");
                        break;
                    case 6:
                        System.out.println("You got Seven Out!");
                        break;
                }
                break;
            case 2:
                switch (die2) {
                    case 1:
                        System.out.println("You got Ace Deuce!");
                        break;
                    case 2:
                        System.out.println("You got Hard Four!");
                        break;
                    case 3:
                        System.out.println("You got Fever Five!");
                        break;
                    case 4:
                        System.out.println("You got Easy Six!");
                        break;
                    case 5:
                        System.out.println("You got Seven Out!");
                        break;
                    case 6:
                        System.out.println("You got Easy Eight!");
                        break;
                }
                break;
            case 3:
                switch (die2) {
                    case 1:
                        System.out.println("You got Easy Four!");
                        break;
                    case 2:
                        System.out.println("You got Fever Five!");
                        break;
                    case 3:
                        System.out.println("You got Hard Six!");
                        break;
                    case 4:
                        System.out.println("You got Seven Out!");
                        break;
                    case 5:
                        System.out.println("You got Easy Eight!");
                        break;
                    case 6:
                        System.out.println("You got Nine!");
                        break;
                }
                break;
            case 4:
                switch (die2) {
                    case 1:
                        System.out.println("You got Fever Five!");
                        break;
                    case 2:
                        System.out.println("You got Easy Six!");
                        break;
                    case 3:
                        System.out.println("You got Seven Out!");
                        break;
                    case 4:
                        System.out.println("You got Hard Eight!");
                        break;
                    case 5:
                        System.out.println("You got Nine!");
                        break;
                    case 6:
                        System.out.println("You got Easy Ten!");
                        break;
                }
                break;
            case 5:
                switch (die2) {
                    case 1:
                        System.out.println("You got Easy Six!");
                        break;
                    case 2:
                        System.out.println("You got Seven Out!");
                        break;
                    case 3:
                        System.out.println("You got Easy Eight!");
                        break;
                    case 4:
                        System.out.println("You got Nine!");
                        break;
                    case 5:
                        System.out.println("You got Hard Ten!");
                        break;
                    case 6:
                        System.out.println("You got Yo-leven!");
                        break;
                }
                break;
            case 6:
                switch (die2) {
                    case 1:
                        System.out.println("You got Seven Out!");
                        break;
                    case 2:
                        System.out.println("You got Easy Eight!");
                        break;
                    case 3:
                        System.out.println("You got Nine!");
                        break;
                    case 4:
                        System.out.println("You got Easy Ten!");
                        break;
                    case 5:
                        System.out.println("You got Yo-leven");
                        break;
                    case 6:
                        System.out.println("You got Boxcars!");
                        break;
                }
                break;
        }
  
    }
    
}