//David Ruiz
//CSE 002
//HW 1

public class WelcomeClass{
  
  public static void main(String arg[]){
    //Prints the first line
    System.out.println("-----------");
    //Prints the second line
    System.out.println("| WELCOME |");
    //Prints the third line
    System.out.println("-----------");
    //Prints the fourth line
    System.out.println("  ^  ^  ^  ^  ^  ^");
    //Prints the fifth line
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    //Prints the sixth line
    System.out.println("<-D--A--R--2--2--2->");
    //Prints the seventh line
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    //Prints the eighth line
    System.out.println("  v  v  v  v  v  v");
  }
}