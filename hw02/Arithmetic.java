//David Ruiz
//CSE 002
//Professor Carr
//The purpose of this program is to calculate the cost, tax, and total cost after tax of shirts, pants, and belts. 

public class Arithmetic {
    
    //Main method required for every Java program
    public static void main(String[] args) {
        
        int numPants = 3; //Number of pairs of pants
        double pantsPrice = 34.98; //Cost per pair of pants

        int numShirts = 2; //Number of sweatshirts
        double shirtPrice = 24.99; //Cost per shirts

        int numBelts = 1; //Number of belts
        double beltCost = 33.99; //Cost per belt

        double paSalesTax = 0.06;//The tax rate

        //Calculations for total costs of each item
        double pantTotalCost = numPants * pantsPrice; //Total cost of pants
        double shirtTotalCost = numShirts * shirtPrice; //Total cost of shirts
        double beltTotalCost = numBelts * beltCost; //Total cost of belts
        
        //Calculations for tax on total costs of each item
        double pantTotalTax = pantTotalCost * paSalesTax; //Total tax on total cost of buying all pants
        double shirtTotalTax = shirtTotalCost * paSalesTax; //Total tax on total cost of buying all shirts
        double beltTotalTax = beltTotalCost * paSalesTax; //Total tax on total cost of buying all belts
        
        //Calculations for total cost of items before tax
        double totalItemCost = pantTotalCost + shirtTotalCost + beltTotalCost; 
        
        //Calculations for total tax on total item cost
        double totalItemTax = totalItemCost * paSalesTax;
        
        //Calculation for total amount paid after taxes of all items 
        double totalPaid = totalItemCost + totalItemTax;
        
        //Rounds values to two decimal places
        double pantTotalTaxRounded =  Math.floor(pantTotalTax * 100) / 100;
        double shirtTotalTaxRounded =  Math.floor(shirtTotalTax * 100) / 100;
        double beltTotalTaxRounded =  Math.floor(beltTotalTax * 100) / 100;
        double totalItemTaxRounded = Math.floor(totalItemTax * 100) / 100;
        double totalPaidRounded = Math.floor(totalPaid * 100) / 100;
        
        
        System.out.println("The total cost of buying all pants is $" + pantTotalCost + " and the total amount of tax paid for all pants is $" + pantTotalTaxRounded + "."); //Print the total cost of each item and the tax paid for buying all of said item
        System.out.println("The total cost of buying all shirts is $" + shirtTotalCost + " and the total amount of tax paid for all shirts is $" + shirtTotalTaxRounded + "."); //Print the total cost of each item and the tax paid for buying all of said item
        System.out.println("The total cost of buying all belts is $" + beltTotalCost + " and the total amount of tax paid for all belts is $" + beltTotalTaxRounded + "."); //Print the total cost of each item and the tax paid for buying all of said item

        System.out.println("The total cost of buying all items before tax is $" + totalItemCost + ", the amount of tax paid is $" + totalItemTaxRounded + ", and the total cost of buying all items after tax is $" + totalPaidRounded + "."); //Print the total cost of all items before tax and after tax
        
    } //End of main method
    
} //End of main class