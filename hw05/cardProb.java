//David Ruiz
//CSE 002
//Professor Carr
//The purpose of this program is to simulate the picking of a 5 card deck, determining if the deck is a four of a kind, a three of a kind, a two pair, or a one pair, and then determining the probability of the aforementioned deck types. 

import java.util.Scanner;

public class cardProb {

    public static void main(String[] args) {
        //Scanner initialization
        Scanner scanner = new Scanner(System.in);
        
        //Variables
        int hands;
        int j;
        int fourkind = 0;
        int threekind = 0;
        int twopair = 0;
        int onepair = 0;
        
        ////////////////////////////////////////////////////////////////////////
        
        //Number of hands
        System.out.println("How many hands would you like?");
        
        while(!scanner.hasNextInt()){
            scanner.next();
            System.out.println("Please enter an integer");
        }
        
        hands = scanner.nextInt();
        
        ////////////////////////////////////////////////////////////////////////
        
        //Main thingy
        for(j = 0; j < hands; j++){
                
            int card1 = 0;
            int card2 = 0;
            int card3 = 0;
            int card4 = 0;
            int card5 = 0;
            
            ////////////////////////////////////////////////////////////////////
                
            if(j == 0){
                card1 = (int)((Math.random() * 52) + 1);
            } else if(j == 1){
                card2 = (int)((Math.random() * 52) + 1);
            } else if(j == 2){
                card3 = (int)((Math.random() * 52) + 1);
            } else if(j == 3){
                card4 = (int)((Math.random() * 52) + 1);
            } else if(j == 4){
                card5 = (int)((Math.random() * 52) + 1);
            }
            
            ////////////////////////////////////////////////////////////////////
                
            while(card1 == card2 || card1 == card3 || card1 == card4 || card1 == card5 || card1 == 0){
                card1 = (int)((Math.random() * 52) + 1);
            }
            while(card2 == card1 || card2 == card3 || card2 == card4 || card2 == card5 || card2 == 0){
                card2 = (int)((Math.random() * 52) + 1);
            }
            while(card3 == card1 || card3 == card2 || card3 == card4 || card3 == card5 || card3 == 0){
                card3 = (int)((Math.random() * 52) + 1);
            }
            while(card4 == card1 || card4 == card2 || card4 == card3 || card4 == card5 || card4 == 0){
                card4 = (int)((Math.random() * 52) + 1);
            }
            while(card5 == card1 || card5 == card2 || card5 == card3 || card5 == card4 || card5 == 0){
                card5 = (int)((Math.random() * 52) + 1);
            }
            
           ///////////////////////////////////////////////////////////////////// 
                
           if(card1 > 13 && card1 < 27){
               card1 -= 13;
           }else if(card1 > 26 && card1 < 40){
               card1 -= 26;
           }else if(card1 > 39 && card1 < 53){
               card1 -= 39;
           }
           
           if(card2 > 13 && card2 < 27){
               card2 -= 13;
           }else if(card2 > 26 && card2 < 40){
               card2 -= 26;
           }else if(card2 > 39 && card2 < 53){
               card2 -= 39;
           }
           
           if(card3 > 13 && card3 < 27){
               card3 -= 13;
           }else if(card3 > 26 && card3 < 40){
               card3 -= 26;
           }else if(card3 > 39 && card3 < 53){
               card3 -= 39;
           }
           
           if(card4 > 13 && card4 < 27){
               card4 -= 13;
           }else if(card4 > 26 && card4 < 40){
               card4 -= 26;
           }else if(card4 > 39 && card4 < 53){
               card4 -= 39;
           }
           
           if(card5 > 13 && card5 < 27){
               card5 -= 13;
           }else if(card5 > 26 && card5 < 40){
               card5 -= 26;
           }else if(card5 > 39 && card5 < 53){
               card5 -= 39;
           }
           
           /////////////////////////////////////////////////////////////////////
           
           //Four of a kind
           if(card2 == card3 && card2 == card4 && card2 == card5){
               fourkind += 1;
           }else if(card1 == card3 && card1 == card4 && card1 == card5){
               fourkind += 1;
           }else if(card1 == card2 && card1 == card4 && card1 == card5){
               fourkind += 1;
           }else if(card1 == card2 && card1 == card3 && card1 == card5){
               fourkind += 1;
           }else if(card1 == card2 && card1 == card3 && card1 == card4){
               fourkind += 1;
           }
           
           //Three of a kind
           if(card3 == card4 && card3 == card5){
               threekind += 1;
           }else if(card1 == card4 && card1 == card5){
               threekind += 1;
           }else if(card1 == card2 && card1 == card5){
               threekind += 1;
           }else if(card1 == card2 && card1 == card3){
               threekind += 1;
           }
           
           //Two pair
           if((card1 == card2) && (card3 == card4 || card4 == card5 || card3 == card5)){
               twopair += 1;
           }else if((card2 == card3) && (card1 == card4 || card1 == card5 || card4 == card5)){
               twopair += 1;
           }else if((card3 == card4) && (card1 == card2 || card1 == card5 || card2 == card5)){
               twopair += 1;
           }else if((card4 == card5) && (card1 == card2 || card1 == card3 || card2 == card3)){
               twopair += 1;
           }
           
           //One pair
           if(card1 == card2 || card1 == card3 || card1 == card4 || card1 == card5){
               onepair += 1;
           }else if(card2 == card3 || card2 == card4 || card2 == card5){
               onepair += 1;
           }else if(card3 == card4 || card3 == card5){
               onepair += 1;
           }else if(card4 == card5){
               onepair += 1;
           }
           
           /////////////////////////////////////////////////////////////////////
           
        }
                    
        //Final statements
        System.out.println("Total number of hands: " + hands + ". Number of loops: " + (hands - 1));
        System.out.println("Probability of a four of a kind: " + ((double)fourkind / (double)hands));
        System.out.println("Probability of a three of a kind: " + ((double)threekind / (double)hands));
        System.out.println("Probability of a two pair: " + ((double)twopair / (double)hands));
        System.out.println("Probability of a one pair: " + ((double)onepair / (double)hands));
        
    }
    
}