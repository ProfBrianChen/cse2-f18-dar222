//David Ruiz
//CSE 002
//Professor Carr
//This program will use a series of if-then/switch statements and a random number generator in order to simulate a random drawing of a card from a deck
public class CardGenerator { //Main class for every program

    public static void main(String[] args) { //Main method for every program
        
        //Variables
        String suit = "";
        String identity = "";
        int number;
        
        //Random number generation
        number = (int)((Math.random() * 52) + 1);
        
        //If-else for suit
        if (number > 0 && number < 14){ 
            suit = "Diamonds";
        } else if (number > 13 && number < 27 ){
            suit = "Clubs";
        } else if (number > 26 && number < 40){
            suit = "Hearts";
        } else if (number > 39 && number < 53){
            suit = "Spades";
        }
        
        //Switch statemnts for the card identity
        switch (number){
            case 1:
            case 14:
            case 27:
            case 40:
                identity = "Ace";
                break;
            case 2:
            case 15:
            case 28:
            case 41:
                identity = "Two";
                break;
            case 3:
            case 16:
            case 29:
            case 42:
                identity = "Three";
                break;
            case 4:
            case 17:
            case 30:
            case 43:
                identity = "Four";
                break;
            case 5:
            case 18:
            case 31:
            case 44:
                identity = "Five";
                break;
            case 6:
            case 19:
            case 32:
            case 45:
                identity = "Six";
                break;
            case 7:
            case 20:
            case 33:
            case 46:
                identity = "Seven";
                break;
            case 8:
            case 21:
            case 34:
            case 47:
                identity = "Eight";
                break;
            case 9:
            case 22:
            case 35:
            case 48:
                identity = "Nine";
                break;
            case 10:
            case 23:
            case 36:
            case 49:
                identity = "Ten";
                break;
            case 11:
            case 24:
            case 37:
            case 50:
                identity = "Jack";
                break;
            case 12:
            case 25:
            case 38:
            case 51:
                identity = "Queen";
                break;
            case 13:
            case 26:
            case 39:
            case 52:
                identity = "King";
                break;
        }
        
        //Prints the number of the card just to make sure, before it tells you the card drawn
        System.out.println("The number of the card drawn was: " + number + ", or in other words, a " + identity + " of " + suit);
        
    } //End of main class
    
} //End of main method