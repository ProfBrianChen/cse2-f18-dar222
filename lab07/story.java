//David Ruiz
//CSE 001
//Professor Carr
//The purpose of this program is to create a story from random words

import java.util.Random;
import java.util.Scanner;

public class story {

    public static void main(String[] args) {
        //Scanner init
        Scanner scanner = new Scanner(System.in);
        
        //Terminal user interaction
        System.out.println("Welcome! Here is your random story.");
        paragraph();
    }
    
    public static String adjective(){
        //Random init
        Random rand = new Random();
        
        //Variables
        int randomInt;
        String adjective = "default";
        
        //Random number
        randomInt = rand.nextInt(10);
        
        //Swtich statements
        switch (randomInt){
            case 0: adjective = "confused";
                    break;
            case 1: adjective = "happy";
                    break;
            case 2: adjective = "sad";
                    break;
            case 3: adjective = "fast";
                    break;
            case 4: adjective = "slow";
                    break;
            case 5: adjective = "inquisitive";
                    break;
            case 6: adjective = "relaxed";
                    break;
            case 7: adjective = "red";
                    break;
            case 8: adjective = "green";
                    break;
            case 9: adjective = "blue";
                    break;
            default: break;
        }
        
        return adjective;
    }
    
    public static String noun(){
        //Random init
        Random rand = new Random();
        
        //Variables
        int randomInt;
        String noun = "default";
        
        //Random number
        randomInt = rand.nextInt(10);
        
        //Swtich statements
        switch (randomInt){
            case 0: noun = "student";
                    break;
            case 1: noun = "dog";
                    break;
            case 2: noun = "cat";
                    break;
            case 3: noun = "kitten";
                    break;
            case 4: noun = "puppy";
                    break;
            case 5: noun = "bird";
                    break;
            case 6: noun = "fox";
                    break;
            case 7: noun = "elephant";
                    break;
            case 8: noun = "rabbit";
                    break;
            case 9: noun = "bunny";
                    break;
            default: break;
        }
        
        return noun;
    }
    
    public static String nounAlt(){
        //Random init
        Random rand = new Random();
        
        //Variables
        int randomInt;
        String nounAlt = "default";
        
        //Random number
        randomInt = rand.nextInt(10);
        
        //Swtich statements
        switch (randomInt){
            case 0: nounAlt = "teacher";
                    break;
            case 1: nounAlt = "car";
                    break;
            case 2: nounAlt = "plane";
                    break;
            case 3: nounAlt = "bus";
                    break;
            case 4: nounAlt = "man";
                    break;
            case 5: nounAlt = "woman";
                    break;
            case 6: nounAlt = "human";
                    break;
            case 7: nounAlt = "robot";
                    break;
            case 8: nounAlt = "building";
                    break;
            case 9: nounAlt = "phone";
                    break;
            default: break;
        }
        
        return nounAlt;
    }
    
    public static String nounAltAlt(){
        //Random init
        Random rand = new Random();
        
        //Variables
        int randomInt;
        String nounAltAlt = "default";
        
        //Random number
        randomInt = rand.nextInt(10);
        
        //Swtich statements
        switch (randomInt){
            case 0: nounAltAlt = "worm";
                    break;
            case 1: nounAltAlt = "fly";
                    break;
            case 2: nounAltAlt = "insect";
                    break;
            case 3: nounAltAlt = "king";
                    break;
            case 4: nounAltAlt = "emperor";
                    break;
            case 5: nounAltAlt = "moth";
                    break;
            case 6: nounAltAlt = "frog";
                    break;
            case 7: nounAltAlt = "virus";
                    break;
            case 8: nounAltAlt = "germ";
                    break;
            case 9: nounAltAlt = "being";
                    break;
            default: break;
        }
        
        return nounAltAlt;
    }
    
    public static String verb(){
        //Random init
        Random rand = new Random();
        
        //Variables
        int randomInt;
        String verb = "default";
        
        //Random number
        randomInt = rand.nextInt(10);
        
        //Swtich statements
        switch (randomInt){
            case 0: verb = "taught";
                    break;
            case 1: verb = "laughed at";
                    break;
            case 2: verb = "hit";
                    break;
            case 3: verb = "jumped on";
                    break;
            case 4: verb = "tickled";
                    break;
            case 5: verb = "talked to";
                    break;
            case 6: verb = "helped";
                    break;
            case 7: verb = "listened to";
                    break;
            case 8: verb = "stared at";
                    break;
            case 9: verb = "passsed";
                    break;
            default: break;
        }
        
        return verb;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    public static String thesis(String noun){
        //Variables
        String adjective = adjective();
        String adjectiveAlt = adjective();
        String verb = verb();
        String nounAlt = nounAlt();
        String space = " ";
        int i;
        String thesis = "";
        String period = ".";
        
        //For loop
        for(i = 0; i < 7; i++){
            switch(i){
                case 0: thesis += "The" + space;
                        break;
                case 1: thesis += adjective + space;
                        break;
                case 2: thesis += noun + space;
                        break;
                case 3: thesis += verb + space;
                        break;
                case 4: thesis += "the" + space;
                        break;
                case 5: thesis += adjectiveAlt + space;
                        break;
                case 6: thesis += nounAlt + period;
                        break;
            }
        }
        return thesis;
    }
    
    public static String action(String noun){
        //Variables
        String adjective = adjective();
        String verb = verb();
        String nounAlt = nounAlt();
        String nounAltAlt = nounAltAlt();
        String space = " ";
        int i;
        String action = "";
        String period = ".";
        
        //Random init
        Random rand = new Random();
        
        //Subject it or noun
        String subject = "placeholder";
        int randomInt = rand.nextInt(2);
        
        switch(randomInt){
            case 0: subject = "It";
                    break;
            case 1: subject = "The" + space + noun;
                    break;
        }
        
        
        //For loop
        for(i = 0; i < 7; i++){
            switch(i){
                case 0: action += subject + space;
                        break;
                case 1: action += verb + space;
                        break;
                case 2: action += "about the" + space;
                        break;
                case 3: action += nounAltAlt + space;
                        break;
                case 4: action += "with the" + space;
                        break;
                case 5: action += adjective + space;
                        break;
                case 6: action += nounAlt + period;
                        break;
            }
        }
        
        return action;
    }
    
    public static String conclusion(String noun){
    //Variables
    int i;
    String conclusion = "";
    String space = " ";
    String verb = verb();
    String nounAlt = nounAlt();
    
    //Loop
    for(i = 0; i < 5; i++){
        switch(i){
            case 0: conclusion += "That" + space;
                    break;
            case 1: conclusion += noun + space;
                    break;
            case 2: conclusion += verb + space;
                    break;
            case 3: conclusion += "its" + space;
                    break;
            case 4: conclusion += nounAlt + "!";
                    break;
                }
        }
        return conclusion;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    public static void paragraph(){
        //Variables
        String paragraph = "";
        int i;
        
        //Random init
        Random rand = new Random();
        
        //Noun for universal use
        String noun = noun();
        
        //Random amount of action, maximum of 5 action sentences
        int actionInt;
        actionInt = rand.nextInt(6);
        
        //If 0, add 1 to insure at least one action sentence
        if(actionInt == 0){
            actionInt += 1;
        }
        
        //Print thesis
        System.out.println(thesis(noun));
        
        //Prints the random amount of action sentences
        for(i = 0; i < actionInt; i++){
            System.out.println(action(noun));
        }
        
        //Prints conclusion
        System.out.println(conclusion(noun));
    }
}