//David Ruiz
//CSE 002
//Professor Carr
//The purpose of this program is to take input from the user about a course they are taking, and making sure that the inputs are correct inputs(i.e. if a integer is asked for, that the user is only able to input integers)

import java.util.Scanner;

public class courseInfo {

    public static void main(String[] args) {
        //Scanner initialization 
        Scanner scanner = new Scanner(System.in);
        
        //Variables
        int coursenumber;
        int timesaweek;
        int students;
        String department;
        String start;
        String prof;
        
        ////////////////////////////////////////////////////////////////////////
        
        //Course number
        System.out.println("What is the course number of the course you are taking?");
                
        while(!scanner.hasNextInt()){
            scanner.next();
            System.out.println("Please enter an integer");
        }
        
        coursenumber = scanner.nextInt();
        
        ////////////////////////////////////////////////////////////////////////
        
        //Department name
        System.out.println("What is the department of the course?");
                
        while(!scanner.hasNext()){
            scanner.next();   
        }
        
        department = scanner.next();
        
        ////////////////////////////////////////////////////////////////////////
        
        //Times a week
        System.out.println("How many times do you meet in a week");
        
        while(!scanner.hasNextInt()){
            scanner.next();
            System.out.println("Please enter an integer");
        }
        
        timesaweek = scanner.nextInt();
        
        ////////////////////////////////////////////////////////////////////////
       
        //Time class starts
        System.out.println("What time does the class begin?");
        scanner.nextLine();
        
        while(!scanner.hasNextLine()){
            scanner.nextLine();   
        }
        
        start = scanner.nextLine();
        
        ////////////////////////////////////////////////////////////////////////
        
        //Professor name
        System.out.println("What is the surname of your professor?");
        
        while(!scanner.hasNext()){
            scanner.nextLine();
        }
        
        prof = scanner.nextLine();
        
       /////////////////////////////////////////////////////////////////////////
       
       //Students
       System.out.println("How many students are in the class?");
       
       while(!scanner.hasNextInt()){
            scanner.next();
            System.out.println("Please enter an integer");
        }
        
        students = scanner.nextInt();
        
        ////////////////////////////////////////////////////////////////////////
        
        //Final statement
        System.out.println("You are taking the course " + coursenumber + " which starts at " + start + " in the " + department + " department, where you meet " + timesaweek + " times a week. The class of " + students + " students is led by Professor " + prof + ".");
        
    }
    
}
