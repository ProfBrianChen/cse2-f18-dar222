//David Ruiz
//CSE 001
//Professor Carr
//This program shows an X amidst a sea of asterisk, the size being dictated by the user

import java.util.Scanner;
public class EncryptedX {

    public static void main(String[] args) {
        //Scanner initialization
        Scanner scanner = new Scanner(System.in);
        
        //Variables
        int size;
        int i;
        int j;
        String row = "";
        String space = " ";
        char thingy = '*';
        
        //Terminal
        System.out.println("What size would you like the square to be?");
       
        //Number of rows
        do {
            System.out.println("Please enter an integer between 1 and 100.");
            while (!scanner.hasNextInt()) {
                System.out.println("Please enter an integer.");
                scanner.next();
            }
            size = scanner.nextInt();
        } while (size < 2 || size > 100);
        
        //Test
        System.out.println("Size: " + size);
        
        //If size is odd
        if(size % 2 == 1){
            //Number of rows printed
            for(i = 1; i <= size; i++){
                //Characters printed in row
                for(j = 1; j <= size; j++){
                    if(i == j || j == (size + 1) - i){
                        thingy = ' ';
                    }     
                    row = row + thingy + space;
                    thingy = '*';
                }
                System.out.println(row);
                row = "";
            }
        }
        
        //If size is even
        if(size % 2 == 0){
            size = size + 1;
             //Number of rows printed
             for(i = 1; i <= size; i++){
                //Characters printed in row
                for(j = 1; j <= size; j++){
                    if(i == j || j == (size + 1) - i){
                        thingy = ' ';
                    }     
                    row = row + thingy + space;
                    thingy = '*';
                }
                System.out.println(row);
                row = "";
            } 
        }   
    }
}