//David Ruiz
//CSE 002
//Professor Carr
//The purpose of this program is to act like a bicycle's cyclometer, in which it records the length in time of a trip, the amount of wheel 
//rotations of a trip, and the total distance travelled of a trip. This data will be collected for two respective trips, and at the end, the 
//total distance travelled of both trips combined will be calculated.

public class Cyclometer {
    
    //Main method required for every Java program
    public static void main(String[] args) {
        
        int secsTrip1=480; //Time elapsed of trip 1
        int secsTrip2=3220; //Time elapsed of trip 2
        int countsTrip1=1561; //Wheel rotations of trip 1
        int countsTrip2=9037; //Wheel roatations of trip 2
        
        double wheelDiameter=27.0; //Diameter of bicycle wheels in inches
        double PI=3.14159; //Value of Pi
        double feetPerMile=5280; //Conversion from miles to feet
        double inchesPerFoot=12; //Conversion from feet to inches
        double secondsPerMinute=60; //Conversion from minutes to seconds
        double distanceTrip1, distanceTrip2,totalDistance;  //Declaring these variables
        
        System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts."); //Prints the time elapsed and number of wheel rotations
        System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts."); //Print the time elapsed and number of wheel rotations  
        
        distanceTrip1=countsTrip1*wheelDiameter*PI; //Distance of trip 1 in inches
        distanceTrip2=countsTrip2*wheelDiameter*PI; //Distance of trip 2 in inches
        
        distanceTrip1/=inchesPerFoot*feetPerMile; //Gives distance in miles
        distanceTrip2/=inchesPerFoot*feetPerMile; //Gives distance in miles
        totalDistance=distanceTrip1+distanceTrip2; //Total distance of trip 1 and trip 2
        
        //Print out the output data.
        System.out.println("Trip 1 was "+distanceTrip1+" miles long."); //Prints the total distance in miles of trip 1
        System.out.println("Trip 2 was "+distanceTrip2+" miles long."); //Prints the total distance in miles of trip 2
        System.out.println("The total distance was "+totalDistance+" miles long."); //Prints the total distance of trip 1 and trip 2 combined


    } //End of main method
    
} //End of class