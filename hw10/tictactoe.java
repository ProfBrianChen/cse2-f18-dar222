//David Ruiz
//CSE 002
//Professor Carr
//This program will act as a tic tac toe game

import java.util.Scanner;

public class tictactoe {

    public static void main(String[] args) {
        //Scanner 
        Scanner scanner = new Scanner(System.in);
        
        //Variables
        boolean finish = false;
        int player = 2; 
        String chip;
        int spot;
        int turn = 0;
        
        //Array
        String[][] board = new String[3][3];
        board[0][0] = "1";
        board[0][1] = "2";
        board[0][2] = "3";
        board[1][0] = "4";
        board[1][1] = "5";
        board[1][2] = "6";
        board[2][0] = "7";
        board[2][1] = "8";
        board[2][2] = "9";
        
        System.out.println("Here is the tic tac toe board. Player 1 is 'O' and Player 2 is 'X'");
        displayBoard(board);
        
        if(finish == false){
            do{
                if(player == 1){
                    chip = "X";
                    player += 1;
                }else{
                    chip = "O";
                    player -= 1;
                }
                
                do{
                    System.out.print("Player " + player + ", choose an integer shown on the board to place your " + chip + ": ");
                    while(!scanner.hasNextInt()){
                        System.out.print("Please enter an integer shown on the board: ");
                        scanner.next();
                    }
                    spot = scanner.nextInt();    
                }
                while(spot <=0 || spot >= 10);
                
                for(int row = 0; row < board.length; row++) {
                    for(int column = 0; column< board[row].length; column++) {
                        if(board[row][column].equals(String.valueOf(spot))){
                            board[row][column] = chip;   
                        }
                    }
                }
                turn +=1;
                displayBoard(board);
                if(checkWin(board) == true){
                    System.out.println("Player " + player + " wins! Thanks for playing!");
                    finish = true;
                }
                if(turn == 9){
                    finish = true;
                    System.out.println("Draw! Thanks for playing!");
                }
            }
            while(finish == false);
        }
    }
    
    public static void displayBoard(String[][] board){
        //Variables
        String space = " ";
        
        for(int row = 0; row < board.length; row++){
            for(int column = 0; column < board[row].length; column++){
                System.out.print(board[row][column] + space);
            }
            System.out.println();
        }
    }
    
    public static boolean checkWin(String[][] board){
        boolean verdict = false;
        
        //Yeah I know, what the hell? Totally not practical! I want multiple if statements individually just for the aesthetic. Beauty is in the eye of the beholder. 
        if((board[0][0] == board[0][1]) && (board[0][1] == board[0][2])){
            verdict = true;
        }
        
        if((board[0][0] == board[1][1]) && (board[1][1] == board[2][2])){
            verdict = true;
        }
        
        if((board[0][0] == board[1][0]) && (board[1][0] == board[2][0])){
            verdict = true;
        }
        
        if((board[1][0] == board[1][1]) && (board[1][1] == board[1][2])){
            verdict = true;
        }
        
        if((board[0][1] == board[1][1]) && (board[1][1] == board[2][1])){
            verdict = true;
        }
        
        if((board[0][2] == board[1][1]) && (board[1][1] == board[2][0])){
            verdict = true;
        }
        
        if((board[0][2] == board[1][2]) && (board[1][2] == board[2][2])){
            verdict = true;
        }
        
        if((board[2][0] == board[2][1]) && (board[2][1] == board[2][2])){
            verdict = true;
        }
      
        return verdict;
    }
       
}